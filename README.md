# A Translation Table to Convert ERA5 Parameter IDs used in DKRZ's data pool into GRIB Codes 

## Description
In DKRZ's data pool on the HPC Levante, ERA5 parameters are referred to as 3-digit parameter ID. 

This 3-digit parameter ID ("PARAM") is derived from the WMO GRIB parameter table. 

PARAM <=256        : 3-digit parameter ID of GRIB table 128  

256 < PARAM <= 512 : 256 + last 3-digits of parameter ID of GRIB table 228

For example, PARAM=259 corresponds to “friction velocity” which has the parameter ID 003 in GRIB table 228.

In the Excel table ERA5ParamsDKRZ_20231030.xlsx in this repository, all ERA5 parameters are listed and explained.

## Usage
This table is meant to decode the parameter IDs used in DKRZ's ERA5 data pool on Levante: /pool/data/ERA5
It also provides an overview of which ERA5 parameters are produced by ECMWF and which ones are available in DKRZ's data pool.
Additional parameters can be added to the data pool upon request. In this case, please write an email to data@dkrz.de.
For more infomation, please see https://docs.dkrz.de/doc/dataservices/finding_and_accessing_data/era_data/index.html

## Support
In case of questions, please contact data@drkz.de

## How to Cite
Heil, A. (2023) A Translation Table to Convert ERA5 Parameter IDs used in DKRZ's data pool into GRIB Codes. Version 1.0. Zenodo. https://doi.org/10.5281/zenodo.10060933

## Acknowledgment
German Cimate Computing Center (DKRZ)

## License
Creative Commons Attribution 4.0 International (CC-BY-4.0)
